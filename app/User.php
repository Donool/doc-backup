<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'tfa_secret',
    ];

    /**
     * Check if the user has 2factor authentication enabled
     *
     * Using this accessor prevents issues with a number as first character for a function
     * @return boolean
     */
    public function getOtpEnabledAttribute()
    {
        return (bool)$this['attributes']['tfa_enabled'];
    }
}
