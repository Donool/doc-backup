<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::auth();
Route::get('/login/verify_token', 'Auth\AuthController@getVerifyToken');
Route::post('/login/verify_token', 'Auth\AuthController@postVerifyToken');

Route::resource('/documents', 'DocumentController');
Route::resource('/categories', 'CategoryController');
Route::resource('/users', 'UserController');
Route::get('/profile', 'UserController@profile');