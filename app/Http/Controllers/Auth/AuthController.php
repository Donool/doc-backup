<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use PragmaRX\Google2FA\Google2FA;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return redirect()->action('Auth\AuthController@getLogin');
    }

    public function getRegister()
    {
        return redirect('login');
    }

    public function showRegistrationForm()
    {
        return redirect('login');
    }

    public function postRegister()
    {
        throw new \Exception('Registration disabled.');
    }

    /**
     * Override the login to implement 2 factor authentication support
     *
     * The code is a copy of the AuthenticatesUsers trait login method but with added checks for 2 factor authentication
     * This allows us to redirect a user to the 2 factor page instead of starting a session immediately if necessary.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        $user = User::whereEmail($credentials['email'])->first();

        // Use Hash::check instead of bcrypt() helper to make sure we can match the passwords!
        if(Hash::check($credentials['password'], $user->password)) {
            // Is 2 factor enabled?
            if($user->tfa_enabled) {
                $request->session()->put('userId', $user->id);
                return redirect()->action('Auth\AuthController@getVerifyToken');
            } else {
                if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
                    flash('Welkom terug ' . $user->name, 'success');
                    return $this->handleUserWasAuthenticated($request, $throttles);
                }
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function getVerifyToken(Request $request)
    {
        if ($request->session()->has('userId')) {
            // we need the userId to authenticate it after checking the 2fa token
            return view('auth.2factor');
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Verify a two factor authentication token
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postVerifyToken(Request $request)
    {
        $user = User::find($request->session()->get('userId'));
        if($user === null) {
            return $this->sendFailedTokenResponse($request);
        }
        $token = $request->only('tfa_token');

        $google2FA = new Google2FA();
        if($google2FA->verifyKey($user->tfa_secret, $token['tfa_token'])) {
            Auth::loginUsingId($user->id);
            flash('Welkom terug ' . $user->name, 'success');
            return $this->handleUserWasAuthenticated($request, true);
        }

        return $this->sendFailedTokenResponse($request);
    }

    /**
     * Get the failed token response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedTokenResponse(Request $request)
    {
        return redirect()->back()
            ->withErrors([
                'tfa_token' => Lang::has('auth.invalid_token') ? Lang::get('auth.invalid_token') : 'Uw token is ongeldig!'
            ]);
    }
}   
