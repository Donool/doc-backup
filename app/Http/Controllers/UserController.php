<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use PragmaRX\Google2FA\Google2FA;

class UserController extends BaseController
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if($user === null) {
            flash('Gebruiker niet gevonden!', 'danger');

            return redirect()->action('UserController@index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if($user === null) {
            flash('Gebruiker niet gevonden!', 'danger');

            return redirect()->action('UserController@index');
        }

        // only allow editing your own user
        if(Auth()->user()->id != (int)$id /*|| Auth()->user->rank === 'admin'*/)
        {
            flash('Geen toegang!', 'danger');
            return redirect()->action('UserController@index');
        }

        $google2fa = new Google2FA();
        if($user->tfa_secret == null) {
            $user->tfa_secret = $google2fa->generateSecretKey(32);
        }
        $user->save();
        $qrCode = $google2fa->getQRCodeInline(
            env('APP_NAME'),
            $user->email,
            $user->tfa_secret
        );

        return view('user.edit', compact('user', 'qrCode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if($user === null) {
            flash('Gebruiker niet gevonden', 'danger');
            return redirect()->action('UserController@index');
        }
         // only edit your own user!
        if(Auth()->user()->id != (int)$id /*|| Auth()->user->rank === 'admin'*/)
        {
            flash('Geen toegang!', 'danger');
            return redirect()->action('UserController@index');
        }

        // enable or disable 2 FA
        $user->tfa_enabled = (bool)$request->get('2fa_enabled');

        $user->save();

        flash('Gebruiker bijgewerkt', 'info');
        return redirect()->back()->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Alias to edit authenticated user
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return $this->edit(Auth::user()->id);
    }
}
