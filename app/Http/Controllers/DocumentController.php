<?php

namespace App\Http\Controllers;

use App\Document;
use ErrorException;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Encryption\EncryptException;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Crypt;


class DocumentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Document::all();

        return view('document.index', compact('documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('document.create', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * TODO: Exception handler
         * TODO: Better folder structure? Use year as basefolder?
         * TODO: Use File/Storage facade or FileSystem class
         * TODO: Use laravel translations?
         */
        $fileName = $request->file('filename')->getClientOriginalName() . '_' . uniqid() . '.' . $request->file('filename')->getClientOriginalExtension();

        $document = Document::create([
            'title'    => $request->get('title'),
            'filename' => $fileName,
        ]);

        $originalContent = file_get_contents($request->file('filename')->getRealPath());
        try {
            $encryptedFile = Crypt::encrypt($originalContent);
            file_put_contents(storage_path('uploads/' . $document->filename), $encryptedFile);
            $document->save();
            flash('Bestand geüpload!', 'info');
        } catch (EncryptException $e) {
            flash('Fout tijdens versleutelen van bestand. Upload geannuleerd!', 'danger');
        }

        return redirect()->action('DocumentController@index');
    }

    /**
     * Download the file
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Document::find($id);
        // decrypt the encrypted file content and return it as attachment (stream)
        try {
            $file = Crypt::decrypt(file_get_contents(storage_path('uploads/' . $document->filename)));
            $headers = [
                'Content-Type' => 'application/octet-stream',
                'Content-Disposition' => 'attachment; filename="'.$document->filename.'"',
                'Content-Length' => strlen($file),
                'Connection' => 'close',
            ];

            $response = \Response::make($file, 200, $headers);
            return $response;
        } catch (DecryptException $e) {
            flash('Fout tijdens ontsleutelen van bestand! Fout: ' . $e->getMessage(), 'danger');
            return redirect()->action('DocumentController@index');
        } catch (ErrorException $e) {
            flash('Fout tijdens laden van bestand! Fout: ' . $e->getMessage(), 'danger');
            return redirect()->action('DocumentController@index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::find($id);
        if($document === null) {
            return response()->redirectToAction('DocumentController@index');
        }

        return view('document.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = $request->file('filename');

        $document = Document::find($id);
        $previousFile = $document->filename;
        $document->title = $request->get('title');
        if($file !== null) {
            $fileName = $request->file('filename')->getClientOriginalName() . '_' . uniqid() . '.' . $request->file('filename')->getClientOriginalExtension();
            $originalContent = file_get_contents($request->file('filename')->getRealPath());
            try {
                $encryptedFile = Crypt::encrypt($originalContent);
                file_put_contents(storage_path('uploads/' . $fileName), $encryptedFile);
                $document->filename = $fileName;
                unlink(storage_path('uploads/' . $previousFile));
            } catch (EncryptException $e) {
                flash('Fout tijdens versleutelen van bestand. Bestand niet gewijzigd!', 'danger');
            }
        }
        $document->save();

        return redirect()->action('DocumentController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = Document::find($id);
        if($document === null)
        {
            flash('Kan het opgevraagde bestand niet vinden!', 'danger');
            return redirect()->action('DocumentController@index');
        }
        $file = storage_path('uploads/' . $document->filename);
        if(file_exists($file)) {
            unlink($file);
        }
        $document->delete();
        flash('Bestand verwijderd');
        return redirect()->action('DocumentController@index');
    }
}
