<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'document';
    protected $guarded = ['id'];

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
}
