@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Gebruikers</div>
                    <div class="panel-body">
                        <div class="pull-right">
                            <a href="{{ action('UserController@create') }}" class="btn btn-primary">Gebruiker toevoegen</a>
                        </div>
                        <table class="table table-striped table-condensed">
                            <thead>
                            <th>ID</th>
                            <th>Naam</th>
                            <th>2FA</th>
                            <th>Opties</th>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>
                                        @if($user->otp_enabled)
                                            Ja
                                        @else
                                            Nee
                                        @endif
                                    </td>
                                    <td>
                                        {{--<a href="{{ action('UserController@show', ['id'=>$user->id]) }}" class="btn btn-default btn-sm">Bekijken</a>--}}
                                        <a href="{{ action('UserController@edit', ['id'=>$user->id]) }}" class="btn btn-default btn-sm">Bewerken</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
