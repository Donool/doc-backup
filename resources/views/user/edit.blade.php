@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Gebruiker bewerken</div>
                    <div class="panel-body">
                        {!! Form::open(['action'=>['UserController@update', $user->id], 'method'=>'post']) !!}
                        {!! Form::hidden('_method', 'PUT') !!}
                        {!! Form::label('name', 'Naam') !!}
                        {!! Form::text('name', $user->name, ['class' => 'form-control', 'required'=>true]) !!}

                        {!! Form::label('email', 'E-mailadres') !!}
                        {!! Form::email('email', $user->email, ['class'=>'form-control']) !!}

                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>2 staps authenticatie</h4>
                                {!! Form::label('2fa_enabled', 'Gebruik authenticatie in twee stappen') !!}
                                {!! Form::hidden('2fa_enabled', 0) !!}
                                {!! Form::checkbox('2fa_enabled', 1) !!}
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p>Gebruik de barcode om uw telefoon te koppelen.</p>
                                <img src="{{ $qrCode }}" class="img-responsive img-thumbnail"
                                     alt="2 staps authenticatie barcode: {{ $user->tfa_secret }}">
                                <br>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p>Of vul handmatig de volgende code in: </p>
                                <p><b>{{ $user->tfa_secret }}</b></p>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="pull-right">
                            {!! Form::submit('Opslaan', ['class'=>'btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
