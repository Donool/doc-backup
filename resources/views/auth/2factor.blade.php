@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Vul uw beveiligingscode in</div>
                <div class="panel-body">
                    <p>Omdat u gebruik maakt van twee staps authenticatie moet u eerst een unieke code invullen.</p>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login/verify_token') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('tfa_token') ? ' has-error' : '' }}">
                            <label for="tfa_token" class="col-md-4 control-label">Token</label>

                            <div class="col-md-6">
                                <input id="tfa_token" type="number" class="form-control" name="tfa_token" value="{{ old('tfa_token') }}">

                                @if ($errors->has('tfa_token'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tfa_token') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Verder
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
