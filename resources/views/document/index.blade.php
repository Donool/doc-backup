@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Documenten</div>
                <div class="panel-body">
                    @if(count($documents) > 0)
                    <div class="pull-right">
                        <a href="{{ action('DocumentController@create') }}" class="btn btn-primary">Document toevoegen</a>
                    </div>
                    <table class="table table-striped table-condensed">
                        <thead>
                            <th>ID</th>
                            <th>Titel</th>
                            <th>Bestandsnaam</th>
                            <th>Opties</th>
                        </thead>
                        <tbody>
                            @foreach($documents as $file)
                                <tr>
                                    <td>{{ $file->id }}</td>
                                    <td>{{ $file->title }}</td>
                                    <td>{{ $file->filename }}</td>
                                    <td>
                                        <a href="{{ action('DocumentController@show', ['id'=>$file->id]) }}" class="btn btn-default btn-sm">Download</a>
                                        <a href="{{ action('DocumentController@edit', ['id'=>$file->id]) }}" class="btn btn-default btn-sm">Bewerken</a>
                                        <a href="{{ action('DocumentController@destroy', ['id'=>$file->id]) }}" class="btn btn-danger btn-sm" data-method="delete" data-confirm="Wil je {{ $file->title }} echt verwijderen?">&times;</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="{{ action('DocumentController@create') }}" class="btn btn-primary">Document toevoegen</a>
                    </div>
                    @else
                        <p>U heeft nog geen bestanden toegevoegd. <a href="{{ action('DocumentController@create') }}" class="btn btn-link">Meteen een bestand toevoegen.</a></p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
