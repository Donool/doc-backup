@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Document bewerken</div>
                <div class="panel-body">
                    {!! Form::open(['action'=>['DocumentController@update', $document->id], 'files'=>true, 'method'=>'post']) !!}
                        {!! Form::hidden('_method', 'PUT') !!}
                        {!! Form::label('title', 'Document titel') !!}
                        {!! Form::text('title', $document->title, array('class' => 'form-control', 'required'=>true)) !!}

                        {!! Form::label('filename', 'Document') !!}
                        {!! Form::file('filename', array('class' => 'form-control')) !!}
                        <br>
                        <div class="pull-right">
                            <a href="{{ action('DocumentController@show', ['id'=>$document->id]) }}" class="btn btn-default">Download bestand</a>
                            {!! Form::submit('Opslaan', ['class'=>'btn btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
