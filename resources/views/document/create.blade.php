@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Document uploaden</div>
                <div class="panel-body">
                    {!! Form::open(['action'=>'DocumentController@store', 'files'=>true]) !!}
                        {!! Form::label('title', 'Document titel') !!}
                        {!! Form::text('title', old('title'), array('class' => 'form-control', 'required'=>true)) !!}

                        {!! Form::label('filename', 'Document') !!}
                        {!! Form::file('filename', array('class' => 'form-control', 'required'=>true)) !!}
                        <br>
                        <div class="pull-right">
                            {!! Form::submit('Opslaan', ['class'=>'btn btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
